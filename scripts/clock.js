// dtermine date
let d = new Date();

// determine size
const size = 96;
const sizeHalf = size / 2;

// determine length of hands
const handWidth = 2;
const handLengthSecond = 36;
const handLengthMinute = 32;
const handLengthHour = 24;

// determine colors
const colorHandSecond = "#34BEF7";
const colorHandMinute = "#F7D334";
const colorHandHour = "#F73478";
const colorCenter = "#4D4D4D";

// initialize canvas
const elementCanvas = document.getElementById('canvas-hands');
const ctx = elementCanvas.getContext('2d');
ctx.translate(sizeHalf, sizeHalf); // translate such that origin :: center

// tick function
setInterval(
	tick,
	1000
);

// initialize
updateDate();
redraw();

// Clock

function updateDate() {
	d = new Date();
	seconds = d.getSeconds();
	minutes = d.getMinutes();
	hours = d.getHours();
}

/**
 * Tick every second.
 */
function tick() {
	// hours
	updateDate();
	redraw();
}

function redraw() {
	ctx.clearRect(-sizeHalf, -sizeHalf, size, size);
	drawHandSecond();
	drawHandMinute();
	drawHandHour();
	drawCenter();
}

// Draw

/**
 * Draw the second hand.
 */
function drawHandSecond() {
	// convert second to angle
	ctx.fillStyle = colorHandSecond;
	let angle = seconds * 6; // convert second to angle; 4 degrees per second
	let pointHand = rotate(0, 0, 0, -handLengthSecond, -angle); // determine the end point of the second hand
	fillLine(0, 0, Math.round(pointHand.x), Math.round(pointHand.y), handWidth);
}

function drawHandMinute() {
	ctx.fillStyle = colorHandMinute;
	let angle = minutes * 6; 	// convert minute to angle; 4 degrees per minute
	let pointHand = rotate(0, 0, 0, -handLengthMinute, -angle); // determine the end point of the second hand
	fillLine(0, 0, Math.round(pointHand.x), Math.round(pointHand.y), handWidth);
}

function drawHandHour() {
	ctx.fillStyle = colorHandHour;
	let angle = 30 * (hours % 12 + minutes / 60); // convert hour to angle; 30 degrees per hour, on a 12 hour clock
	let pointHand = rotate(0, 0, 0, -handLengthHour, -angle); // determine the end point of the second hand
	fillLine(0, 0, Math.round(pointHand.x), Math.round(pointHand.y), handWidth);
}

function drawCenter() {
	ctx.fillStyle = colorCenter;
	ctx.fillRect(-2, -2, 4, 4);
}

// Utility

/* draw a line using the Bresenham algorithm */
function fillLine(x0, y0, x1, y1, width) {
	var dx = Math.abs(x1 - x0);
	var dy = Math.abs(y1 - y0);
	var sx = (x0 < x1) ? 1 : -1;
	var sy = (y0 < y1) ? 1 : -1;
	var err = dx - dy;

	while (true) {
		ctx.fillRect(x0, y0, width, width);

		if ((x0 === x1) && (y0 === y1)) break;
		var e2 = 2 * err;
		if (e2 > -dy) { err -= dy; x0 += sx; }
		if (e2 < dx) { err += dx; y0 += sy; }
	}
}

/**
 * Rotate a point about an origin.
 * @param angleDegrees Counter-clockwise, in degrees.
 */
function rotate(cx, cy, x, y, angleDegrees) {
	var radians = (Math.PI / 180) * angleDegrees,
		cos = Math.cos(radians),
		sin = Math.sin(radians),
		nx = (cos * (x - cx)) + (sin * (y - cy)) + cx,
		ny = (cos * (y - cy)) - (sin * (x - cx)) + cy;
	return { x: nx, y: ny };
}